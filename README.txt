﻿$Id$

Description
-----------

The og_teampage module allows to configure and present a table where the members of the group are shown with picture, real name and any description given by group admin. The table can be configured to show within the group node (og_mission) or by a filter in any node at any place.


Installation
------------

1) you need the 'og module' working to get 'og_teampage' working

2) upload og_teampage.module, og_teampage.install, og_teampage.info and no_pic.png into a folder under '/modules'

3) enable the 'og_teampage module' in drupal by visiting 'admin/build/modules'

4) visit 'admin/og/og_teampage' to set some settings

5) visit 'admin/settings/filters' to enable the 'og group members table' filter if you want to 

Usage
--------------------------------------------

1) in not done, create a group at your site

2) you'll find a link 'configure team table view' in the tabs of your og node, follow this links to upload the pictures, write descriptions etc.

3) select 'show teamtable on group home page' to present the table directly at bottom of your group node or ...

4) in any node (the og homepage node or a node within the group for example) insert '[teamtable:xxx]' where xxx stands for the group id of the group you want to present. The group id is the node id of the groups homepage node.
